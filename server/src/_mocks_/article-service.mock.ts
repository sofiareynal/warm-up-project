export const mockArticlesCreate = {
  _id: '64764a71f9a44a9015beb6fb',
  created_at: Date(),
  author: 'Sofia R.',
  deleted_at: null,
  story_title: null,
  title: 'How to test the warm up project',
  url: null,
  story_url: 'https://www.google.com',
  __v: 0,
};

export const mockArticleDeleted = {
  _id: '64764a71f9a44a9015beb6fb',
  created_at: Date(),
  author: 'Sofia R.',
  deleted_at: Date(),
  story_title: null,
  title: 'How to test the warm up project',
  url: null,
  story_url: 'https://www.google.com',
  __v: 0,
};

export const mockArticlesData = {
  author: 'Sofia R.',
  title: 'How to test the warm up project',
  story_url: 'https://www.google.com',
  url: null,
  story_title: null,
  deleted_at: null,
  created_at: null,
};

export const mockArticlesFoundData = {
  articles: [
    {
      _id: '64764a71f9a44a9015beb6fb',
      created_at: Date(),
      author: 'Sofia R.',
      deleted_at: null,
      story_title: null,
      title: 'How to test the warm up project',
      url: null,
      story_url: 'https://www.google.com',
      __v: 0,
    },
    {
      _id: '64764a71f9a44a9015beb6fc',
      created_at: Date(),
      author: 'Sofia R.',
      deleted_at: null,
      story_title: null,
      title: 'How to test the warm up project part 2',
      url: null,
      story_url: 'https://www.google.com',
      __v: 0,
    },
  ],
  count: 2,
};
