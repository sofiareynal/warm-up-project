export const configLoader = () => {
  return {
    port: process.env.PORT,
    mongo: process.env.MONGO_URI,
    api: process.env.API_URL,
    database: process.env.DATABASE,
  };
};
