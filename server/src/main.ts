import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const port = configService.get('port');
  const frontEndUrl = configService.get('frontend_url');
  app.enableCors({
    origin: [frontEndUrl, 'http://localhost:3000'],
    methods: 'GET, DELETE',
  });
  await app.listen(port);
}
bootstrap();

