import { Controller, Get, Param, Delete, Query } from '@nestjs/common';
import { ArticleService } from '../services/article.service';
import { PaginationQueryDto } from '../dto/pagination.dto';

@Controller('articles')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  getArticles(@Query() paginationQuery: PaginationQueryDto) {
    return this.articleService.findAll(paginationQuery);
  }
  @Get(':id')
  getOneArticle(@Param('id') id: string) {
    return this.articleService.findOne(id);
  }

  @Delete(':id')
  deleteArticle(@Param('id') id: string) {
    return this.articleService.delete(id);
  }
}
