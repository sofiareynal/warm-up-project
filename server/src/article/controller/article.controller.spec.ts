import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { ArticleService } from '../services/article.service';
import { ArticleRepository } from '../repository/article.repository';
import { getModelToken } from '@nestjs/mongoose';
import { Article } from '../schemas/article.schema';

describe('ArticleController', () => {
  let controller: ArticleController;
  // let articleService: ArticleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [
        ArticleService,
        ArticleRepository,
        {
          provide: getModelToken(Article.name),
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
