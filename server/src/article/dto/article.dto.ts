import { IsString } from 'class-validator';

export class CreateArticleDto {
  @IsString()
  readonly title: string;

  @IsString()
  readonly story_title: string;

  @IsString()
  readonly url: string;

  @IsString()
  readonly story_url: string;

  @IsString()
  readonly author: string;

  @IsString()
  readonly created_at: string;

  @IsString()
  readonly deleted_at: string;
}
