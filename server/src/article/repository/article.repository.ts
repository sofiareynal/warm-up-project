import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from '../schemas/article.schema';
import { Model } from 'mongoose';
import { CreateArticleDto } from '../dto/article.dto';
import { PaginationQueryDto } from '../dto/pagination.dto';

@Injectable()
export class ArticleRepository {
  constructor(
    @InjectModel(Article.name) private readonly articleModel: Model<Article>,
  ) {}

  create(createArticleDto: CreateArticleDto): Promise<Article> {
    const article = this.articleModel
      .findOneAndUpdate(
        { created_at: createArticleDto.created_at }, // can I do this with an Id without adding it to the Dto
        createArticleDto,
        { upsert: true, new: true },
      )
      .exec();
    return article;
  }

  async findAll(
    paginationQuery: PaginationQueryDto,
  ): Promise<{ articles: Article[]; count: number }> {
    const { limit, offset } = paginationQuery;
    const filters = [
      {
        $or: [{ title: { $ne: null } }, { story_title: { $ne: null } }],
      },
      {
        $or: [{ url: { $ne: null } }, { story_url: { $ne: null } }],
      },
    ];
    const articlesPromise = this.articleModel
      .find({
        deleted_at: null,
        $and: filters,
      })
      .skip(offset)
      .limit(limit)
      .sort({ created_at: 'desc' })
      .exec();

    const countPromise = this.articleModel.countDocuments({
      deleted_at: null,
      $and: filters,
    });

    const [articles, count] = await Promise.all([
      articlesPromise,
      countPromise,
    ]);
    return { articles, count };
  }

  findOne(id: string): Promise<Article> {
    const article = this.articleModel.findOne({ _id: id }).exec();
    if (!article) {
      throw new NotFoundException(`Article #${id} not found.`);
    }
    return article;
  }

  delete(id: string): Promise<Article> {
    const articleDeleted = this.articleModel
      .findOneAndUpdate({ _id: id }, { deleted_at: new Date() }, { new: true })
      .exec();

    if (!articleDeleted) {
      throw new NotFoundException(`Article #${id} not found.`);
    }

    return articleDeleted;
  }
}
