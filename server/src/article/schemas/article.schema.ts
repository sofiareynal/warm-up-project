import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema() // marks a class as a schema definition, the collection name in mongo will be 'articles'
export class Article extends Document {
  @Prop() // defines a property in the document
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;

  @Prop()
  author: string;

  @Prop()
  created_at: string;

  @Prop({ default: null })
  deleted_at: Date;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
