// import { ArticleService } from './article.service';
import { Test, TestingModule } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Model, connect, Connection } from 'mongoose';
import { Article, ArticleSchema } from '../schemas/article.schema';
import { ArticleService } from './article.service';
import { ArticleRepository } from '../repository/article.repository';
import { getModelToken } from '@nestjs/mongoose';
import {
  mockArticleDeleted,
  mockArticlesCreate,
  mockArticlesData,
  mockArticlesFoundData,
} from '../../_mocks_/article-service.mock';
import { PaginationQueryDto } from '../dto/pagination.dto';

let mongod: MongoMemoryServer;
let mongoConnection: Connection;
let articleModel: Model<Article>;

let service: ArticleService;
let repository: ArticleRepository;

describe('ArticleService', () => {
  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    articleModel = mongoConnection.model(Article.name, ArticleSchema);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        ArticleRepository,
        {
          provide: getModelToken(Article.name),
          useValue: articleModel,
        },
      ],
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    repository = module.get<ArticleRepository>(ArticleRepository);
  });

  afterEach(async () => {
    const collections = mongoConnection.collection;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  describe(' Article Service Test', () => {
    it('should create articles', async () => {
      repository.create = jest.fn().mockReturnValueOnce(mockArticlesCreate);

      const article = await service.create(mockArticlesData);

      expect(article).toBeDefined();
      expect(article).toEqual(mockArticlesCreate);
    });

    it('should get all articles', async () => {
      repository.findAll = jest.fn().mockReturnValue(mockArticlesFoundData);
      const paginationQuery: PaginationQueryDto = {
        offset: 0,
        limit: 5,
      };

      const article = await service.findAll(paginationQuery);

      expect(article).toBeDefined();
      expect(article).toEqual(mockArticlesFoundData);
      expect(repository.findAll).toHaveBeenCalledWith(paginationQuery);
    });

    it('should get one article by id', async () => {
      repository.findOne = jest.fn().mockReturnValueOnce(mockArticlesCreate);

      const id = '64764a71f9a44a9015beb6fb';
      const article = await service.findOne(id);

      expect(article).toBeDefined();
      expect(article).toEqual(mockArticlesCreate);
      expect(repository.findOne).toHaveBeenCalledWith(id);
    });

    it('should delete one article by id', async () => {
      repository.delete = jest.fn().mockReturnValueOnce(mockArticleDeleted);

      const id = '64764a71f9a44a9015beb6fb';
      const article = await service.delete(id);

      expect(article).toBeDefined();
      expect(article).toEqual(mockArticleDeleted);
      expect(repository.delete).toHaveBeenCalledWith(id);
    });
  });
});
