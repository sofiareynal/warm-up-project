import { Injectable } from '@nestjs/common';
import { Article } from '../schemas/article.schema';
import { CreateArticleDto } from '../dto/article.dto';
import { PaginationQueryDto } from '../dto/pagination.dto';
import { ArticleRepository } from '../repository/article.repository';

@Injectable()
export class ArticleService {
  constructor(private readonly articleRepository: ArticleRepository) {}

  create(createArticleDto: CreateArticleDto): Promise<Article> {
    return this.articleRepository.create(createArticleDto);
  }

  findAll(
    paginationQuery: PaginationQueryDto,
  ): Promise<{ articles: Article[]; count: number }> {
    return this.articleRepository.findAll(paginationQuery);
  }

  findOne(id: string): Promise<Article> {
    return this.articleRepository.findOne(id);
  }

  delete(id: string): Promise<Article> {
    return this.articleRepository.delete(id);
  }
}
