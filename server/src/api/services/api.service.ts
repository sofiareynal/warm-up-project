import { ArticleService } from '../../article/services/article.service';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { map, firstValueFrom } from 'rxjs';
import { CreateArticleDto } from 'src/article/dto/article.dto';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ApiService implements OnModuleInit {
  constructor(
    private readonly articleService: ArticleService,
    private readonly http: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.create();
  }

  apiFindAll(): Promise<object[]> {
    // didn't work with Observable<AxiosResponse<object[]>> -- with this, I didn't need firstValueFrom
    const api = this.configService.get<string>('api');
    const response = this.http
      .get(api)
      .pipe(map((response: AxiosResponse) => response.data.hits));
    return firstValueFrom(response); // converts an Observable into a Promise
  }

  @Cron(CronExpression.EVERY_HOUR)
  async create() {
    const response = await this.apiFindAll();
    return await Promise.all(
      response.map(
        (data: CreateArticleDto) => this.articleService.create(data),
        // console.log('holi')
      ),
    );
  }
}
