import { Controller, Get } from '@nestjs/common';
import { ApiService } from '../services/api.service';

@Controller('api')
export class ApiController {
  constructor(private readonly appService: ApiService) {}

  @Get()
  apiFindAll() {
    return this.appService.apiFindAll();
  }

  @Get('apis')
  create() {
    return this.appService.create();
  }
}
