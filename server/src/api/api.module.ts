import { Module } from '@nestjs/common';
import { ApiController } from './controller/api.controller';
import { ApiService } from './services/api.service';
import { HttpModule } from '@nestjs/axios';
import { ArticleModule } from 'src/article/article.module';

@Module({
  imports: [HttpModule, ArticleModule],
  controllers: [ApiController],
  providers: [ApiService],
})
export class ApiModule {}
