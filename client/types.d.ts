type Article = {
    '_id': string,
    'created_at': string,
    'title': string,
    'story_title': string,
    'author': string,
    'url': string,
    'story_url': string,
    'deleted_at': string
}