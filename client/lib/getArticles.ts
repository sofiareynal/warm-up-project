export default async function getArticles(offset:number, limit:number): Promise<{ articles: Article[]; count: number }> {
  const url = process.env.NEXT_PUBLIC_URL;
  const res = await fetch (`${url}?offset=${offset}&limit=${limit}`, {cache: "no-store"});
  if (!res.ok) throw new Error("Failed to fetch articles");

  return res.json();
}
