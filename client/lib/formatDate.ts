import dayjs from "dayjs";
import calendar from "dayjs/plugin/calendar";

dayjs.extend(calendar);

export default function formatDate(data: string): string {
  return dayjs(data).calendar(null, {
    sameDay: "h:mm a",
    lastDay: "[Yesterday]",
    lastWeek: "MMM DD",
    sameElse: "MMM DD",
  });
}
