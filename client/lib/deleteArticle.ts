export default async function deleteArticle(id: string) {
  const url = process.env.NEXT_PUBLIC_URL;
  const res = await fetch(`${url}/${id}`, {
    method: "DELETE",
  });
  if (!res.ok) throw new Error("Failed to delete article");
  return res;
}
