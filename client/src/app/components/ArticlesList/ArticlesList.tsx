"use client";
import { useState } from "react";
import deleteArticle from "../../../../lib/deleteArticle";
import getArticles from "../../../../lib/getArticles";
import Pagination from "../Pagination/Pagination";
import ArticleCard from "../ArticleCard/ArticleCard";
import styles from "./articlesList.module.css";

export default function ArticleList({
  data,
  count,
}: {
  data: Article[];
  count: number;
}) {
  const [currentArticles, setCurrentArticles] = useState<Article[]>(data);
  const [currentCount, setCurrentCount] =useState<number>(count);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const articlesPerPage = 5;

  const onDelete = async (id: string) => {
    try {
      await deleteArticle(id);
      const newOffset = (currentPage - 1) * articlesPerPage;
      const { articles, count } = await getArticles(newOffset, articlesPerPage);

      if (articles.length === 0 && currentPage > 1) {
        const newPage = currentPage - 1;
        setCurrentPage(newPage);
        const newOffset = (newPage - 1) * articlesPerPage;
        const { articles, count } = await getArticles(newOffset, articlesPerPage);
        setCurrentArticles(articles);
        setCurrentCount(count);
      } else {
        setCurrentArticles(articles);
        setCurrentCount(count);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const paginationHandler = async (page: number) => {
    try {
      setCurrentPage(page);
      const newOffset = (page - 1) * articlesPerPage;
      const { articles, count } = await getArticles(newOffset, articlesPerPage);
      setCurrentArticles(articles);
      setCurrentCount(count);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <section className={styles.container}>
        {currentArticles.map((article: Article) => {
          return (
            <ArticleCard key={article._id} data={article} onDelete={onDelete} />
          );
        })}
      </section>
      <footer className={styles.footer}>
        <Pagination currentCount={currentCount} paginationHandler={paginationHandler} />
      </footer>
    </>
  );
}
