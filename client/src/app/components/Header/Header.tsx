import styles from "./header.module.css"

export default function Header() {
  return (
    <header className={styles.header}>
      <h1 className={styles.title}>HN Feed</h1>
      <h3>We {'<3'} hacker news!</h3>
    </header>
  );
}
