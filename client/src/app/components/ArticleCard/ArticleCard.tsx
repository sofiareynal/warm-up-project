"use client";
import Link from "next/link";
import formatDate from "../../../../lib/formatDate";
import styles from "./articleCard.module.css";
import Image from "next/image";
import deleteIcon  from '../../../../public/delete.svg';
import { useState } from "react";

export default function ArticleCard({
  data,
  onDelete,
}: {
  data: Article;
  onDelete: Function;
}) {

  const [isHovered, setIsHovered] = useState<boolean>(false);

  const handleHover = () => {
    setIsHovered(true);
  }

  const handleNotHover = () => {
    setIsHovered(false);
  }

  return (
    <div className={styles.container} onMouseEnter={handleHover} onMouseLeave={handleNotHover}>
      <Link className={styles.title}href={data.url || data.story_url} target="_blank">
        {data.title || data.story_title}
      </Link>
      <h4 className={styles.author}>{`- ${data.author} -`}</h4>
      <h4 className={styles.date}>{formatDate(data.created_at)}</h4>
      {isHovered && 
      <button className={styles.delete} onClick={() => onDelete(data._id)}> 
        <Image src={deleteIcon} alt={'Delete article'} width={20} height={20}/> 
      </button>
      }
    </div>
  );
}
