"use client";

import { useState, useEffect } from "react";
import styles from "./pagination.module.css";

export default function Pagination({
  currentCount,
  paginationHandler,
}: {
  currentCount: number;
  paginationHandler: Function;
}) {
  const [isActive, setIsActive] = useState<number>(1);
  const articlesPerPage = 5;
  const totalPages = Math.ceil(currentCount / articlesPerPage);
  const pageNumbers: number[] = [];

  for (let i: number = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    if (isActive > totalPages && totalPages > 0) {
      setIsActive(totalPages);
      paginationHandler(totalPages);
    }
  }, [currentCount, isActive, paginationHandler, totalPages]);

  return (
    <>
      {pageNumbers.map((page, index) => {
        return (
          <button
            className={isActive === page ? styles.btnActive : styles.btn}
            key={index}
            value={page}
            onClick={() => {
              paginationHandler(page);
              setIsActive(page);
            }}
          >
            {page}
          </button>
        );
      })}
    </>
  );
}
