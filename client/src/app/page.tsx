import type { Metadata } from "next";
import styles from "./page.module.css";
import getArticles from "../../lib/getArticles";
import ArticlesList from "./components/ArticlesList/ArticlesList";
import Header from "./components/Header/Header";

export const metadata: Metadata = {
  title: "Warm Up Project",
};

export default async function ArticlesPage() {
  const articlesData: Promise<{ articles: Article[]; count: number }> =
    getArticles(0, 5);
  const { articles, count } = await articlesData;

  const content = (
    <main className={styles.container}>
      <Header />
      <ArticlesList data={articles} count={count} />
    </main>
  );
  return content;
}
