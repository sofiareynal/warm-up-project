
# Warm Up Project - Hacker News
This app will show you recently posted articles about Node.js on Hacker News. You will also be able to delete the ones you want.



## Technologies Used
FrontEnd: Next js, React js, CSS.

BackEnd: Node js, Nest js, MongoDB, Mongoose.

Extras: Docker, Git.

## Installation

```bash
  git clone https://gitlab.com/sofiareynal/warm-up-project.git

  create env files on both client and server folders

  cd warm-up-project

  docker-compose up
```
    
## Preview

![App Screenshot](https://i.postimg.cc/6q22Kds3/Hacker-preview.png)
![App Screenshot](https://i.postimg.cc/VNhKmP5W/Screenshot-2023-06-02-101720.png)


